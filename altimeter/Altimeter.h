#ifndef _ALTIMETER_H
#define _ALTIMETER_H

#include <boost/interprocess/managed_shared_memory.hpp>
#include <deque>
#include <string>

#include "../../../SerialCommunication/SerialConnection.h"

using namespace boost::interprocess;
using namespace std;

class Altimeter
{
  private:
	float altitude;

  public:
	long baudRate;
	std::string devicePort;
	deque<char> dataQueue;
	//deque<string> dataQueue;
	int minimum_queue_size;
	bool DataResponse;
	int DataRate;

	//managed_shared_memory *sharedMemoryObject;

	Altimeter(std::string devicePort, long baudRate);
	~Altimeter();
	SerialConnection Serial;

	static void Callback(const char *dataString, size_t dataStringLength, void *thisClassObject);
	//static void Callback(string dataString, size_t dataStringLength, void *thisClassObject);
	void dataProcess();

	void parseDataPacket(std::string dataString);

	double get_Altitude();
};

#endif
