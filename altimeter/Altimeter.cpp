#include <iostream>
#include "Altimeter.h"

using namespace std;

Altimeter::Altimeter(std::string devicePort, long baudRate)
{
	this->minimum_queue_size = 7;
	this->devicePort = devicePort;
	this->baudRate = baudRate;
	this->DataResponse = false;
	this->DataRate = 0;

	this->altitude = 0;

	this->Serial.Initialize(this->devicePort, this->baudRate, this->Callback, this);
}

Altimeter::~Altimeter()
{
}

// void Altimeter::Callback(const char *dataString, size_t dataStringLength, void *thisClassObject)
// {
// 	int idx = 0;
// 	while (idx < dataStringLength)
// 	{
// 		((Altimeter *)thisClassObject)->dataQueue.push_back(dataString[idx++]);
// 	}

// 	if (((Altimeter *)thisClassObject)->dataQueue.size() > ((Altimeter *)thisClassObject)->minimum_queue_size)
// 	{
// 		((Altimeter *)thisClassObject)->dataProcess();
// 	}
// }

void Altimeter::Callback(const char* dataString, size_t dataStringLength, void *thisClassObject)
{

	((Altimeter *)thisClassObject)->dataQueue.push_back(dataString);
	
	if (((Altimeter *)thisClassObject)->dataQueue.size() > ((Altimeter *)thisClassObject)->minimum_queue_size)
	{
		((Altimeter *)thisClassObject)->dataProcess();
	}
}

// void Altimeter::dataProcess()
// {
// 	std::string dataPacket = "";
// 	while (this->dataQueue.front() != '$')
// 	{
// 		this->dataQueue.pop_front();
// 	}
// 	while (this->dataQueue.front() != 'M')
// 	{
// 		dataPacket.push_back(this->dataQueue.front());
// 		this->dataQueue.pop_front();
// 	}
// 	this->parseDataPacket(dataPacket);
// }

void Altimeter::dataProcess()
{
	std::string dataPacket="";
	if(this->dataQueue.size()>0){
		while (this->dataQueue.front() != 'm')
		{
			dataPacket.push_back(this->dataQueue.front());
			this->dataQueue.pop_front();
		}
		this->altitude = dataPacket;
	}
}

// void Altimeter::parseDataPacket(std::string dataString)
// {
// 	try
// 	{
// 		std::string value = "";
// 		if (dataString.size() >= 24)
// 		{
// 			for (int i = 0; i < 6; i++)
// 			{
// 				value.push_back(dataString.at(i + 17));
// 			}
// 			this->altitude = std::stod(value);
// 		}
// 		else
// 		{
// 			std::cout << "Altimeter Short String: " << dataString << "---" << dataString.size() << std::endl;
// 			// this->dataQueue.clear();
// 		}
// 	}
// 	catch (std::exception &e)
// 	{
// 		std::cout << "[Altimeter][parseDataPacket][ERROR] - "
// 				  << e.what() << std::endl;
// 	}
// }

double Altimeter::get_Altitude()
{
	return this->altitude;
}
