#ifndef _UBLOXGPS_H
#define _UBLOXGPS_H

#include <boost/interprocess/managed_shared_memory.hpp>
#include <deque>
#include <string>

#include "../../../SerialCommunication/SerialConnection.h"

using namespace boost::interprocess;
using namespace std;

class UBloxGPS
{
  private:
	float altitude;

  public:
	long baudRate;
	std::string devicePort;
	deque<char> dataQueue;
	int minimum_queue_size;
	bool DataResponse;
	int DataRate;

	//managed_shared_memory *sharedMemoryObject;

	UBloxGPS(std::string devicePort, long baudRate);
	~UBloxGPS();
	SerialConnection Serial;

	static void Callback(const char *dataString, size_t dataStringLength, void *thisClassObject);
	void dataProcess();

	void parseDataPacket(std::string dataString);

};

#endif
