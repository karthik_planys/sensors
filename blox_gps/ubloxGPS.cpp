#include <iostream>
#include "ubloxGPS.h"

using namespace std;

UBloxGPS::UBloxGPS(std::string devicePort, long baudRate)
{
	this->minimum_queue_size = 50;
	this->devicePort = devicePort;
	this->baudRate = baudRate;
	this->DataResponse = false;
	this->DataRate = 0;

	this->altitude = 0;

	this->Serial.Initialize(this->devicePort, this->baudRate, this->Callback, this);
}

UBloxGPS::~UBloxGPS()
{
}

void UBloxGPS::Callback(const char *dataString, size_t dataStringLength, void *thisClassObject)
{
	int idx = 0;
	while (idx < dataStringLength)
	{
		((UBloxGPS *)thisClassObject)->dataQueue.push_back(dataString[idx++]);
	}

	if (((UBloxGPS *)thisClassObject)->dataQueue.size() > ((UBloxGPS *)thisClassObject)->minimum_queue_size)
	{
		((UBloxGPS *)thisClassObject)->dataProcess();
	}
}


void UBloxGPS::dataProcess()
{
	std::string dataPacket = "";
	// while (this->dataQueue.front() != '$')
	// {
	// 	this->dataQueue.pop_front();
	// }
	while (this->dataQueue.front() != '$')
	{
		dataPacket.push_back(this->dataQueue.front());
		this->dataQueue.pop_front();
	}
	this->parseDataPacket(dataPacket);
}


void UBloxGPS::parseDataPacket(std::string dataString)
{
	try
	{
		if(dataString.size() >0){
			std::string delimiter = ",";
            size_t pos = 0;
            std::string token;
            while ((pos = dataString.find(delimiter)) != std::string::npos) {
                token = dataString.substr(0, pos);
                dataString.erase(0, pos + delimiter.length());
            }
		}
	}
	catch (std::exception &e)
	{
		std::cout << "[UBloxGPS][parseDataPacket][ERROR] - "
				  << e.what() << std::endl;
	}
}


